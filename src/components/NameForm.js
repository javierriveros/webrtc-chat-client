import React from "react";

export default function NameForm({ user, setUser }) {
  const [isEditing, setIsEditing] = React.useState(true);
  return (
    <div className="flex flex-col my-2">
      {isEditing ? (
        <form
          onSubmit={(e) => {
            e.preventDefault();
            setIsEditing(false);
          }}
        >
          <div className="flex flex-col">
            <label className="block w-full text-sm font-medium leading-5 text-gray-700">
              Nombre
            </label>
            <div className="flex w-full mt-1 rounded-md shadow-sm">
              <input
                className="flex-1 block w-full transition duration-150 ease-in-out rounded-none form-input rounded-l-md sm:text-sm sm:leading-5"
                placeholder="Escriba su nombre"
                value={user}
                onChange={(e) => setUser(e.target.value)}
              />
              <button
                className="items-center px-4 py-2 text-sm font-medium leading-5 text-white transition duration-150 ease-in-out bg-indigo-600 border border-transparent rounded-none shadow-sm rounded-r-md hover:bg-indigo-500 focus:outline-none focus:shadow-outline-blue active:bg-indigo-600"
                type="button"
                onClick={() => setIsEditing(false)}
              >
                <svg
                  className="w-4 h-4"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M5 13l4 4L19 7"
                  />
                </svg>
              </button>
            </div>
          </div>
        </form>
      ) : (
        <div className="flex flex-col">
          <p className="block w-full text-sm font-medium leading-5 text-gray-700">
            Nombre
          </p>
          <div className="flex items-center">
            <p className="block w-full text-sm font-medium leading-5 text-gray-700">
              {user}
            </p>
            <button
              type="button "
              onClick={() => setIsEditing(true)}
              className="inline-flex justify-center px-4 py-2 mr-2 text-sm leading-4 text-gray-500 transition duration-150 ease-in-out bg-white border border-gray-300 rounded-md hover:text-gray-400 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue"
            >
              Editar
            </button>
          </div>
        </div>
      )}
    </div>
  );
}
