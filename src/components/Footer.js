import React from "react";

export default function Footer() {
  return (
    <div>
      <div className="w-full mt-2 border-t border-gray-300"></div>
      <p className="px-2 mt-2 text-sm text-center text-gray-500 bg-white">
        Integrantes: <br />
        Walter Javier Riveros Castro <br /> Juan David Celemín
      </p>
    </div>
  );
}
