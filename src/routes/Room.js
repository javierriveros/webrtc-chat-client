import React, { useRef, useEffect, useState } from "react";
import io from "socket.io-client";
import { Link } from "react-router-dom";
import NameForm from "../components/NameForm";
import Footer from "../components/Footer";

const Room = ({ match }) => {
  const peerRef = useRef();
  const socketRef = useRef();
  const otherUser = useRef();
  const sendChannel = useRef();
  const [text, setText] = useState("");
  const [messages, setMessages] = useState([]);
  const [user, setUser] = useState("Nuevo Usuario");
  const [error, setError] = useState(null);
  const { roomID } = match.params;

  useEffect(() => {
    socketRef.current = io.connect(process.env.REACT_APP_API_URL);
    socketRef.current.emit("join room", roomID);

    socketRef.current.on("other user", (userID) => {
      // call user function
      peerRef.current = createPeer(userID);
      sendChannel.current = peerRef.current.createDataChannel("sendChannel");
      sendChannel.current.onmessage = handleReceiveMessage;
      // end call user function

      otherUser.current = userID;

      setMessages((messages) => [
        ...messages,
        {
          yours: false,
          type: "announcement",
          value: `Nuevo usuario conectado ${userID}`,
        },
      ]);
    });

    socketRef.current.on("user joined", (userID) => {
      setMessages((messages) => [
        ...messages,
        {
          yours: false,
          type: "announcement",
          value: `Nuevo usuario conectado ${userID}`,
        },
      ]);
      otherUser.current = userID;
    });

    socketRef.current.on("offer", handleOffer);

    socketRef.current.on("answer", handleAnswer);

    socketRef.current.on("ice-candidate", handleNewICECandidateMsg);
  }, []);

  function handleReceiveMessage(e) {
    const message = JSON.parse(e.data);

    setMessages((messages) => [
      ...messages,
      {
        yours: false,
        type: "message",
        value: message.text,
        user: message.user,
      },
    ]);
  }

  function createPeer(userID) {
    const peer = new RTCPeerConnection({
      iceServers: [
        {
          urls: "stun:stun.stunprotocol.org",
        },
        {
          urls: "turn:numb.viagenie.ca",
          credential: "muazkh",
          username: "webrtc@live.com",
        },
      ],
    });

    peer.onicecandidate = handleICECandidateEvent;
    peer.onnegotiationneeded = () => handleNegotiationNeededEvent(userID);

    return peer;
  }

  function handleNegotiationNeededEvent(userID) {
    peerRef.current
      .createOffer()
      .then((offer) => {
        return peerRef.current.setLocalDescription(offer);
      })
      .then(() => {
        const payload = {
          target: userID,
          caller: socketRef.current.id,
          sdp: peerRef.current.localDescription,
        };
        socketRef.current.emit("offer", payload);
      })
      .catch((e) => console.log(e));
  }

  function handleOffer(incoming) {
    peerRef.current = createPeer();
    peerRef.current.ondatachannel = (event) => {
      sendChannel.current = event.channel;
      sendChannel.current.onmessage = handleReceiveMessage;
    };
    const desc = new RTCSessionDescription(incoming.sdp);
    peerRef.current
      .setRemoteDescription(desc)
      .then(() => {})
      .then(() => {
        return peerRef.current.createAnswer();
      })
      .then((answer) => {
        return peerRef.current.setLocalDescription(answer);
      })
      .then(() => {
        const payload = {
          target: incoming.caller,
          caller: socketRef.current.id,
          sdp: peerRef.current.localDescription,
        };
        socketRef.current.emit("answer", payload);
      });
  }

  function handleAnswer(message) {
    const desc = new RTCSessionDescription(message.sdp);
    peerRef.current.setRemoteDescription(desc).catch((e) => console.log(e));
  }

  function formatPriority(priority) {
    return [priority >> 24, (priority >> 8) & 0xffff, priority & 0xff].join(
      " | "
    );
  }

  function handleICECandidateEvent(e) {
    if (e.candidate) {
      const payload = {
        target: otherUser.current,
        candidate: e.candidate,
      };
      socketRef.current.emit("ice-candidate", payload);
      setMessages((messages) => [
        ...messages,
        {
          yours: false,
          type: "announcement",
          value: `Nuevo ice candidate: component: ${
            e.candidate.component
          } type: ${e.candidate.type} foundation: ${
            e.candidate.foundation
          } protocol: ${e.candidate.protocol} address: ${
            e.candidate.address
          } port: ${e.candidate.port} priority: ${formatPriority(
            e.candidate.priority
          )}`,
        },
      ]);
    }
  }

  function handleNewICECandidateMsg(incoming) {
    const candidate = new RTCIceCandidate(incoming);

    peerRef.current.addIceCandidate(candidate).catch((e) => console.log(e));
  }

  function handleChange(e) {
    setText(e.target.value);
  }

  function sendMessage(e) {
    e.preventDefault();
    if (!text || !text.length) return;
    try {
      sendChannel.current.send(JSON.stringify({ text, user }));
      setMessages((messages) => [
        ...messages,
        { yours: true, type: "message", value: text, user },
      ]);
      setText("");
    } catch (error) {
      console.log({ error });
      setError("Error al enviar el mensaje");
    }
  }

  function renderMessage(message, index) {
    if (message.type == "announcement") {
      return (
        <div className="flex justify-center w-full px-2 pb-4 mt-2" key={index}>
          <p className="px-4 py-2 mb-2 mr-2 text-sm text-teal-500 bg-teal-100 rounded-md">
            {message.value}
          </p>
        </div>
      );
    }
    if (message.yours) {
      return (
        <div className="flex justify-end w-full px-2 pb-4 mt-2" key={index}>
          <div className="px-4 py-2 mb-2 mr-2 text-black bg-white rounded-md rounded-br-none shadow-md rounded-tl-0">
            <span className="block text-sm text-gray-500">Tú</span>
            {message.value}
          </div>
        </div>
      );
    }

    return (
      <div className="flex justify-start w-full px-2 pb-4 mt-2" key={index}>
        <div
          className="px-4 py-2 mb-2 mr-2 text-white rounded-md rounded-bl-none shadow-md rounded-tl-0"
          style={{ background: "linear-gradient(-135deg, #1de9b6, #1dc4e9)" }}
        >
          <span className="block text-sm text-gray-100">{message.user}</span>
          {message.value}
        </div>
      </div>
    );
  }

  return (
    <div className="w-full max-w-xl max-h-screen min-h-screen mx-auto">
      <div className="flex flex-row items-center content-between my-1">
        <Link
          to="/"
          className="inline-flex justify-center px-4 py-2 mr-2 text-sm leading-4 text-gray-500 transition duration-150 ease-in-out bg-white border border-gray-300 rounded-md hover:text-gray-400 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            className="w-4 h-4 mr-2"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M10 19l-7-7m0 0l7-7m-7 7h18"
            />
          </svg>
          Volver al inicio
        </Link>
        <p className="flex-1 px-2 py-2 m-0 text-sm text-center text-blue-600 bg-blue-100 rounded-md shadow-sm">
          {roomID}
        </p>
      </div>
      <NameForm user={user} setUser={setUser} />
      {error && (
        <p className="p-0 mb-1 text-sm leading-5 text-red-600">{error}</p>
      )}
      <div
        className="min-h-full p-4 overflow-auto border border-gray-200 rounded-md"
        style={{ minHeight: "65vh", maxHeight: "65vh" }}
      >
        {messages.map(renderMessage)}
      </div>
      <form onSubmit={sendMessage}>
        <div className="flex w-full mt-2 rounded-md shadow-sm">
          <input
            className="flex-1 w-full transition duration-150 ease-in-out rounded-none form-input rounded-l-md sm:text-sm sm:leading-5"
            placeholder="Escriba un mensaje"
            value={text}
            onChange={handleChange}
          />

          <button
            className="items-center px-4 py-2 text-sm font-medium leading-5 text-white transition duration-150 ease-in-out bg-indigo-600 border border-transparent rounded-none shadow-sm rounded-r-md hover:bg-indigo-500 focus:outline-none focus:shadow-outline-blue active:bg-indigo-600"
            type="submit"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              className="w-4 h-4"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M9 5l7 7-7 7"
              />
            </svg>
          </button>
        </div>
      </form>
      <Footer />
    </div>
  );
};

export default Room;
