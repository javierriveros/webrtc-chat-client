import React from "react";
import slugify from "../slugify";
import Footer from "../components/Footer";

const CreateRoom = (props) => {
  const [name, setName] = React.useState("");
  const [error, setError] = React.useState(null);
  const [roomID, setRoomID] = React.useState(null);

  function create() {
    if (!name || !name.length) {
      setError("El nombre no puede estar vacío");
      return;
    }

    props.history.push(`/room/${roomID}`);
  }

  return (
    <div className="max-w-2xl mx-auto mt-4">
      <div className="mt-5 md:mt-0 md:col-span-2">
        <div className="shadow sm:rounded-md sm:overflow-hidden">
          <div className="px-4 py-5 bg-white sm:p-6">
            <h3 className="text-lg font-medium leading-6 text-gray-900">
              Crear sala
            </h3>
            <form
              onSubmit={(e) => {
                e.preventDefault();
                create();
              }}
            >
              <div className="my-4">
                <label
                  htmlFor="company_website"
                  className="block text-sm font-medium leading-5 text-gray-700"
                >
                  Nombre
                </label>
                <div className="flex mt-1 rounded-md shadow-sm">
                  <span className="inline-flex items-center px-3 text-sm text-gray-500 border border-r-0 border-gray-300 rounded-l-md bg-gray-50">
                    {window.location.href}
                  </span>
                  <input
                    className="flex-1 block w-full transition duration-150 ease-in-out rounded-none form-input rounded-r-md sm:text-sm sm:leading-5"
                    placeholder="nombre-sala"
                    onChange={(e) => {
                      setName(e.target.value);
                      setRoomID(`${slugify(e.target.value)}`);
                    }}
                    value={name}
                  />
                </div>
                {error && (
                  <p className="mt-2 text-sm leading-5 text-red-600">{error}</p>
                )}

                <p className="p-1 my-2 text-center text-gray-500 border border-gray-300 rounded-md bg-gray-50">
                  {window.location.href}
                  {"room/"}
                  {roomID ? roomID : ""}
                </p>
              </div>
              <span className="inline-flex rounded-md shadow-sm">
                <button
                  type="submit"
                  className="inline-flex justify-center px-4 py-2 text-sm font-medium leading-5 text-white bg-indigo-600 border border-transparent rounded-md hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700"
                >
                  Crear sala
                </button>
              </span>
            </form>
          </div>
        </div>
        <Footer />
      </div>
    </div>
  );
};

export default CreateRoom;
